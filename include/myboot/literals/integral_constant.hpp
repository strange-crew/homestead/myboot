#pragma once
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/hana.hpp>
#pragma GCC diagnostic pop

namespace homestead::myboot::literals::integral {


template < int C >
using constant = boost::hana::integral_constant<int, C>;


template < char ... Ct >
[[nodiscard]] constexpr auto operator""_i()
{
    return constant<boost::hana::literals::operator "" _c<Ct...>().value>{};
}


}
