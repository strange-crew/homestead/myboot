#pragma once

#include "integral_constant.hpp"

namespace homestead::myboot::literals::bytes {

template < char ... Cs >
constexpr auto operator "" _k() 
{ 
    using namespace integral;
    return operator "" _i<Cs...>() * 1024_i;
}


template < char ... Cs >
constexpr auto operator "" _m() 
{
    using namespace integral;
    return operator "" _k<Cs...>() * 1024_i;
}


template < char ... Cs >
constexpr int operator "" _g() 
{ 
    using namespace integral;
    return operator "" _m<Cs...>() * 1024_i;
}


}
