#pragma once


#include "../storage/partition_description.hpp"
#include "../storage/device_description.hpp"
#include "../literals/integral_constant.hpp"

#include <array>

namespace homestead::myboot::hal
{

template < typename PartitionTableList >
struct system_description
{
    PartitionTableList starting_layout;
};


template < typename Board >
constexpr auto build_hal(Board board)
{
    using namespace literals::integral;
    // collect devices from peripherals and from mcu
    // create empty tables
    // return result
    return system_description // but for know it's known.
    {
        .starting_layout = storage::partition_table_list
        {
            storage::partition_table{ .device = board.mcu.storage_devices[0_i] }
        }
    };
}


template < typename MockDriver, typename SystemDescription >
constexpr auto mock_storage(SystemDescription description)
{
    return system_description
    {
        .starting_layout = boost::hana::fold
            (
                description.starting_layout.tuple,
                storage::partition_table_list{},
                [](auto list, auto part_tbl) constexpr
                {
                    return list.append(
                            storage::partition_table
                            {
                                .device = storage::device_description
                                {
                                    .id = part_tbl.device.id,
                                    .sectors = part_tbl.device.sectors,
                                    .write_align = part_tbl.device.write_align,
                                    .driver = MockDriver{ }
                                }
                            }
                        )
                    ;
                }
            )
    };
}


}
