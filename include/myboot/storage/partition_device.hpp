#pragma once


namespace homestead::myboot::storage {

#if 0
template < typename Device, partition_description partition >
struct device_partition
{
    device_partition(Device & device) : device_(device) {}

    static constexpr int total_size()
    {
        constexpr auto device = Device::device_description();
        int sum = 0;
        for (auto i = partition.sector_start; i < partition.sector_next; ++i)
            sum += device.sectors[i].size;
        return sum;
    }


    static constexpr int offset()
    {
        constexpr auto sector = Device::device_description().sectors[partition.sector_start];
        return sector.offset;
    }


    template < typename Buffer >
    void write(int offset, Buffer const& buffer)
    {
        constexpr auto sector = Device::device_description().sectors[partition.sector_start];
        device_.write(sector.offset + offset, buffer);
    }


    template < typename Buffer >
    void read(int offset, Buffer & buffer)
    {
        constexpr auto sector = Device::device_description().sectors[partition.sector_start];
        device_.read(sector.offset + offset, buffer);
    }


    void erase_sector(int index)
    {
        device_.erase_sector(index + partition.sector_start);
    }


    void erase_all()
    {
        constexpr auto sector_list = Device::device_description().sectors;
        for (auto i = partition.sector_start; i < partition.sector_next; ++i)
            erase_sector(i - partition.sector_start);
    }

private:
    Device & device_;
};

template < typename Device, partition_table ptable >
struct partitioned_device
{
    partitioned_device(Device & device)
        : device_(device)
    {}


    template < int bytes >
    auto append_partition() const
    {
        constexpr auto new_table = ptable.template append_partition<bytes>();
        return partitioned_device<Device, new_table>{device_};
    }

    static constexpr int partition_count()
    {
        return ptable.partition_count();
    }

    template < int index >
    auto get() 
    {
        constexpr auto description = Device::device_description();
        constexpr auto partition = ptable.template get<index>();
        return device_partition<Device, partition>(device_);
    }


private:
    Device & device_;

};


template < typename Device >
auto partition_device(Device & device)
{
    constexpr auto desc = Device::device_description();
    constexpr auto table = partition_table<desc>{};
    return partitioned_device<Device, table>(device);
}

#endif
}

