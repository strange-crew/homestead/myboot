#pragma once

namespace homestead::myboot::storage::operators {

template < typename StorageA, typename StorageB >
struct assignment
{
    assignment(StorageA * storage_a, StorageB const* storage_b)
        : a(storage_a)
        , b(storage_b)
    {}

    StorageA * a;
    StorageB const* b;
};


}
