#pragma once
#include "device_description.hpp"
#include "sector_list.hpp"

#include "../literals/integral_constant.hpp"
#include "../meta/list.hpp"

namespace homestead::myboot::storage {


template < typename DeviceDescription, typename StartSector, typename EndSector >
struct partition_description
{
    DeviceDescription device;
    StartSector start_sector;
    EndSector end_sector;


    static constexpr auto sum_sectors()
    {
        using namespace literals::integral;
        return boost::hana::fold
            (
                boost::hana::make_range(StartSector{}, EndSector{} + 1_i),
                0_i,
                [](auto sum, auto index)
                {
                    return sum + DeviceDescription{}.sectors[index].size;
                }
            )
        ;
    }

    static constexpr auto byte_size = sum_sectors();
    static constexpr auto sector_count = EndSector{} - StartSector{} + literals::integral::constant<1>{};
    static constexpr auto offset = DeviceDescription{}.sectors[StartSector{}].offset;

    static constexpr auto thisn() { return partition_description{}; }

    template < typename Buffer >
    static void write(int offset, Buffer const& buffer)
    {
        thisn().device.write(
                offset + thisn().offset,
                buffer
            )
        ;
    }

    template < typename Buffer >
    static void read(int offset, Buffer & buffer)
    {
        thisn().device.read(offset + thisn().offset, buffer);
    }

    static void erase_all()
    {
        for (auto i = thisn().start_sector.value; i < thisn().end_sector.value; ++i)
            thisn().device.erase_sector(i);
    }
};

template < typename ... Partition >
struct partition_list : meta::list<partition_list<Partition...>>
{
    constexpr partition_list(Partition...) {}
    constexpr partition_list() {}

};
template < >
struct partition_list<> : meta::list<partition_list<>> {};

template < typename Device, typename AvailableSectors = decltype(Device{}.sectors), typename PartitionList = partition_list<> >
struct partition_table
{
    Device device;
    AvailableSectors available_sectors;// = AvailableSectors{}; //Device{}.sectors;
    PartitionList partitions; // = PartitionList{};

    static constexpr auto available_bytes = AvailableSectors{}.byte_size;
    static constexpr auto reserved_bytes = Device{}.sectors.byte_size - available_bytes;

    template < typename Dev, typename AS, typename P >
    using apply = partition_table<Dev, AS, P>;

    template < typename Index >
    constexpr auto operator [] (Index index) const
    {
        return partitions[index];
    }


    static constexpr auto thisn() { return partition_table{}; }

    static constexpr auto append_partition()
    {
        return append_partition(available_bytes);
    }


    template < typename Bytes >
    static constexpr auto append_partition(Bytes bytes)
    {
        using namespace literals::integral;

        constexpr auto start_sector = thisn().available_sectors[0_i].index;
        constexpr auto end_sector = boost::hana::fold
            (
                thisn().available_sectors.tuple,
                boost::hana::make_tuple(0_i, 0_i), 
                [bytes](auto idx_sum, auto sector) constexpr
                {
                    return boost::hana::if_
                    (
                        idx_sum[1_i] >= bytes,
                        idx_sum,
                        boost::hana::make_tuple(sector.index, sector.size + idx_sum[1_i])
                    );
                }
            )[0_i]
        ;

        constexpr auto partition = partition_description
        {
            .device = thisn().device,
            .start_sector = start_sector,
            .end_sector = end_sector
        };

        constexpr auto remaining_sectors = boost::hana::fold
            (
                boost::hana::drop_while
                (
                    thisn().available_sectors.tuple,
                    [end = partition.end_sector](auto sector) constexpr
                    {
                        return sector.index <= end;
                    }
                ),
                sector_list{},
                [](auto fold, auto sector) constexpr
                {
                    return fold.append(sector);
                }
            )
        ;


        return partition_table::apply
        {
            .device = thisn().device,
            .available_sectors = remaining_sectors,
            .partitions = thisn().partitions.append(partition)
        };
    }
};


template < typename ... PartitionTables >
struct partition_table_list : meta::list<partition_table_list<PartitionTables...>>
{
    constexpr partition_table_list(PartitionTables ...) {}
    constexpr partition_table_list() {}
};

template <>
struct partition_table_list<> : meta::list<partition_table_list<>> 
{
    constexpr partition_table_list() {}
};


#if 0
template < typename Device, typename SectorStart, typename SectorEnd >
struct partition_description
{
    Device device;
    SectorStart sector_start;
    SectorEnd sector_end;
};

template < typename Partition, typename Next >
struct partition_list
{
    Partition partition;
    Next next;
};

struct part_end {};

template < typename DeviceDescription, typename Partitions = part_end >
struct partition_table_t
{
    DeviceDescription device;
    Partitions partitions;

    template < typename DD, typename P >
    using partition_table_t_t = partition_table_t<DD, P>;

    template < typename Bytes >
    static constexpr auto append_partition(Bytes bytes)
    {
        using namespace literals::integral;
        constexpr auto tpl = DeviceDescription{}.sectors.reserve_from(Partitions{}.partition.sector_end.index() + 1_i, bytes);
        constexpr auto start = std::get<0>(tpl);
        constexpr auto end = std::get<1>(tpl);
        return partition_table_t_t
        {
            .device = DeviceDescription{},
            .partitions = partition_list
            {
                .partition = partition_description
                {
                    .device = DeviceDescription{},
                    .sector_start = start,
                    .sector_end = end
                },
                .next = Partitions{}
            }
        };
    }

    static constexpr auto reserved_size()
    {
        return Partitions{}.partition.sector_end.offset()
             + Partitions{}.partition.sector_end.size()
        ;
    }


    static constexpr auto available_size()
    {
        return DeviceDescription::total_size() - reserved_size();
    }
};


template < typename DeviceDescription >
struct partition_table_t<DeviceDescription, part_end>
{
    DeviceDescription device;

    template < typename DD, typename P >
    using partition_table_t_t = partition_table_t<DD, P>;

    static constexpr auto reserved_size()
    {
        using namespace literals::integral;
        return 0_i;
    }

    static constexpr auto available_size()
    {
        return DeviceDescription::total_size();
    }


    template < typename Bytes >
    static constexpr auto append_partition(Bytes bytes)
    {
        using namespace literals::integral;
        constexpr auto tpl = DeviceDescription{}.sectors.reserve_from(0_i, bytes);
        constexpr auto start = std::get<0>(tpl);
        constexpr auto end = std::get<1>(tpl);
        return partition_table_t_t
        {
            .device = DeviceDescription{},
            .partitions = partition_list
            {
                .partition = partition_description
                {
                    .device = DeviceDescription{},
                    .sector_start = start,
                    .sector_end = end
                },
                .next = part_end{}
            }
        };
    }
};

template < typename device_description_t >
using partition_table = partition_table_t<device_description_t, part_end>;
#endif

}
