#pragma once

#include "device_description.hpp"
#include "operators/assignment.hpp"
#include <array>

//#include "../meta/render_list.hpp"
#include "../literals/integral_constant.hpp"


namespace homestead::myboot::storage {

template < typename DeviceDescription >
struct memory_storage_device
{
    DeviceDescription description;

    void erase_all()
    {
        std::fill(std::begin(memory), std::end(memory), 0xFF);
    }

    void erase_sector(int i)
    {
        std::fill(
                std::begin(memory) + sectors[i].begin,
                std::begin(memory) + sectors[i].end,
                0xFF
            )
        ;
    }


    template < typename Buffer >
    void read(int offset, Buffer & buf) const
    {
        auto start = std::begin(memory) + offset;
        auto end = start + buf.size();

        std::copy(start, end, std::begin(buf));
    }

    template < typename Buffer >
    void write(int offset, Buffer const& buf)
    {
        if (offset % description.write_align != 0) return;

        auto wptr = std::begin(memory) + offset;

        for (auto && ch : buf)
            *wptr++ &= ch;
    }

#if 0
    template < typename OtherStorage >
    auto operator = (OtherStorage const& other)
    {
        return operators::assignment(this, &other);
    }

    auto operator = (memory_storage_device const& other)
    {
        return operators::assignment(this, &other);
    }
#endif

    std::array<unsigned char, DeviceDescription::total_size> memory;

    struct sector
    {
        int begin;
        int end;
    };
    std::array<sector, DeviceDescription{}.sectors.sector_count.value> const sectors = boost::hana::unpack(
            DeviceDescription{}.sectors.tuple,
            [](auto ... sectors) constexpr 
            { 
                return std::array<sector, DeviceDescription{}.sectors.sector_count.value>
                {{
                    { .begin = sectors.offset, .end = sectors.offset + sectors.size } ...
                }};
            }
        )
    ;

#if 0
    struct sector { int begin; int end; };
    static constexpr auto sectors = meta::render_list_f(
            DeviceDescription{}.sectors,
            [](auto s) constexpr { return sector{ .begin = s.offset.value, .end = s.offset.value + s.size.value }; }
        )
    ;
#endif
};


template < typename DeviceDescription >
struct memory_holder
{
    DeviceDescription device;

    static memory_storage_device<DeviceDescription> storage;
};

template < typename DeviceDescription >
memory_storage_device<DeviceDescription> memory_holder<DeviceDescription>::storage;


struct memory_storage_driver
{
    template < typename Device >
    static void erase_all(Device)
    {
        auto && storage = memory_holder<Device>::storage;
        storage.erase_all();
    }
        
    template < typename Device >
    static void erase_sector(Device, int sector)
    {
        auto && storage = memory_holder<Device>::storage;
        storage.erase_sector(sector);
    }

    template < typename Device, typename Buffer >
    static void read(Device, int offset, Buffer & buffer)
    {
        auto && storage = memory_holder<Device>::storage;
        storage.read(offset, buffer);
    }

    template < typename Device, typename Buffer >
    static void write(Device, int offset, Buffer const& buffer)
    {
        auto && storage = memory_holder<Device>::storage;
        storage.write(offset, buffer);
    }
};

}
