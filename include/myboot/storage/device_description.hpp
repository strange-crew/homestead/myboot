#pragma once

#include "../meta/list.hpp"
 
namespace homestead::myboot::storage {

template < typename Id, typename SectorList, typename WriteAlign, typename Driver >
struct device_description
{
    Id id;
    SectorList sectors;
    WriteAlign write_align;
    Driver driver;

    static constexpr auto total_size = SectorList::byte_size;

    static device_description thisn() { return device_description{}; }

    static void erase_all()
    {
        thisn().driver.erase_all(thisn());
    }

    static void erase_sector(int sector)
    {
        thisn().driver.erase_sector(thisn(), sector);
    }

    template < typename Buffer >
    static void read(int offset, Buffer & buffer)
    {
        thisn().driver.read(thisn(), offset, buffer);
    }

    template < typename Buffer >
    static void write(int offset, Buffer const& buffer)
    {
        thisn().driver.write(thisn(), offset, buffer);
    }
};


template < typename ... DeviceDescription >
struct device_list : meta::list<device_list<DeviceDescription...>>
{
    constexpr device_list(DeviceDescription ... ) {}
    constexpr device_list() {}
};

template < >
struct device_list<> : meta::list<device_list<>> {};

}
