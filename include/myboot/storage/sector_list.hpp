#pragma once

#include "../literals/integral_constant.hpp"
#include "../meta/list.hpp"

namespace homestead::myboot::storage {


template < typename Index, typename Offset, typename Size >
struct sector_description
{
    Index index;
    Offset offset;
    Size size;

    using index_t = Index;
    using offset_t = Offset;
    using size_t = Size;

};


template < typename ... SectorN >
struct sector_list : meta::list<sector_list<SectorN...>>
{
    constexpr sector_list(SectorN ...) {}
    constexpr sector_list() {}

    constexpr static auto sector_count = literals::integral::constant<sizeof ... (SectorN)>{};
    constexpr static auto byte_size = boost::hana::fold
        (
            boost::hana::make_tuple(SectorN{}...),
            literals::integral::constant<0>{},
            [](auto sum, auto sector) constexpr
            {
                return sum + sector.size;
            }
        )
    ;
};

template < >
struct sector_list<> : meta::list<sector_list<>>
{
    constexpr static auto sector_count = literals::integral::constant<0>{};
    constexpr static auto byte_size = literals::integral::constant<0>{};
};


template < typename SectorSize, typename SectorCount >
struct even_sector_list
{
    SectorSize sector_size;
    SectorCount sector_count;

    constexpr auto operator()() const
    {
        return boost::hana::fold
        (
            boost::hana::make_range(literals::integral::constant<0>{}, even_sector_list{}.sector_count),
            sector_list{},
            [](auto slist, auto n) constexpr
            {
                return slist.append(
                        sector_description
                        {
                            .index = n,
                            .offset = n * even_sector_list{}.sector_size,
                            .size = even_sector_list{}.sector_size
                        }
                    )
                ;
            }
        );
    }
};

    
}
