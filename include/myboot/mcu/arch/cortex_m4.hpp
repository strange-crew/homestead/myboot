#pragma once


namespace homestead::myboot::mcu::arch
{

struct cortex_m4_t
{
};

constexpr auto cortex_m4 = cortex_m4_t{};


}
