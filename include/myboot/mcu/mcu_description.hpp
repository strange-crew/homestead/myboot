#pragma once


namespace homestead::myboot::mcu {


template < typename Arch, typename StorageDevices >
struct mcu_description
{
    Arch arch;
    StorageDevices storage_devices;
};


}
