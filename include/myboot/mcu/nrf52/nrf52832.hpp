#pragma once


#include "../mcu_description.hpp"
#include "../arch/cortex_m4.hpp"
#include "../../storage/drivers/flashpage.hpp"
#include "../../storage/device_description.hpp"
#include "../../storage/sector_list.hpp"

namespace homestead::myboot::mcu
{

using namespace literals::integral;

constexpr auto nrf52832 = mcu_description
{
    .arch = arch::cortex_m4,
    .storage_devices = storage::device_list
    {
        storage::device_description
        {
            .id = 0_i,
            .sectors = storage::even_sector_list
            {
                .sector_size = 4096_i,
                .sector_count = 128_i
            }(),
            .write_align = 4_i,
            .driver = storage::drivers::flashpage
        }
    },
};


}
