#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/hana.hpp>
#pragma GCC diagnostic pop

#include "../literals/integral_constant.hpp"

namespace homestead::myboot::meta {



template < typename Derived >
struct list;

template < template < typename ...> typename Derived, typename ... ItemN >
struct list<Derived<ItemN...>>
{

    constexpr static auto tuple = boost::hana::tuple<ItemN...>{};

    template < typename ... Added >
    static constexpr auto append(Added ... )
    {
        return Derived<ItemN..., Added...>{};
    }

    template < typename Index >
    constexpr auto operator [] (Index index) const
    {
        static_assert(index.value < sizeof ... (ItemN), "Index out of range");
        return tuple[index];
    }

    constexpr static int size = literals::integral::constant<sizeof ... (ItemN)>{};
};



}
