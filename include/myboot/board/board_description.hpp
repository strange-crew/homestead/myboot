#pragma once


namespace homestead::myboot::boards {


template < typename Mcu, typename Devices >
struct board_description
{
    Mcu mcu;
    Devices peripherals;
};


}
