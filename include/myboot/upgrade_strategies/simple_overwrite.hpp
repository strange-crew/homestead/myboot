#pragma once

#include "../storage/operators/assignment.hpp"
#include "../storage/device_description.hpp"

namespace homestead::myboot::upgrade_strategies {

template < typename SectorsA, typename SectorsB >
constexpr int calculate_buffer_size(storage::device_description<SectorsA> dst, storage::device_description<SectorsB> src)
{
    return dst.write_align * 4;
}


template < typename StorageA, typename StorageB >
void simple_overwrite(storage::operators::assignment<StorageA, StorageB> && assignment)
{
    static_assert(StorageA::device_description().total_size() >= StorageB::device_description().total_size());

    auto && dst = *assignment.a;
    auto && src = *assignment.b;

    dst.erase_all();

    std::array<unsigned char, calculate_buffer_size(StorageA::device_description(), StorageB::device_description())> buffer;
    for (auto i = 0; i < dst.total_size(); i += buffer.size())
    {
        src.read(i, buffer);
        dst.write(i, buffer);
    }
}


}
