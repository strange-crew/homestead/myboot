#pragma once



namespace homestead::myboot::image {


template < 
    typename BootImage,
    typename AppImages
>
struct image_table
{
    BootImage boot;
    AppImages applications;
};


}
