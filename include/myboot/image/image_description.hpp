#pragma once

#include "../meta/list.hpp"


namespace homestead::myboot::image {

template < typename PrimaryPartition >
struct image_description
{
    PrimaryPartition primary;
};


template < typename ... ImageDescriptions >
struct image_list : meta::list<image_list<ImageDescriptions...>>
{
    constexpr image_list(ImageDescriptions...) {}
    constexpr image_list() {}
};


template <>
struct image_list<> : meta::list<image_list<>> {};


}
