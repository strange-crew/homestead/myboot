#!/bin/bash

docker run -it --rm \
    -v $(pwd):/work \
    -v $(pwd)/.home:/home/dev-user \
    -u dev-user \
    arm-build
