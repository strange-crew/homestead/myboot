#!/bin/bash

git clone git@gitlab.com:dot-files3/tmux ~/.tmux
git clone git@gitlab.com:dot-files3/vim ~/.vim

ln -s ~/.tmux/tmux.conf ~/.tmux.conf
ln -s ~/.vim/vimrc ~/.vimrc

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim +PluginInstall +qall
pushd ~/.vim/bundle/YouCompleteMe
python3 install.py
popd

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

