#include <boost/ut.hpp>
#include <myboot/image/image_description.hpp>
#include <myboot/storage/device_description.hpp>
#include <myboot/storage/memory_storage_device.hpp>
#include <myboot/storage/sector_list.hpp>
#include <myboot/storage/partition_description.hpp>
#include <myboot/storage/partition_device.hpp>
#include <myboot/hal/system_description.hpp>
#include <myboot/image/image.hpp>
#include <myboot/image/image_table.hpp>

#include <myboot/literals/integral_constant.hpp>
#include <myboot/literals/byte_literals.hpp>

using namespace homestead::myboot;
using namespace literals::integral;
using namespace literals::bytes;

boost::ut::suite image_description_suite = []
{
    constexpr auto device = storage::device_description
    {
        .id = 0_i,
        .sectors = storage::even_sector_list
        {
            .sector_size = 10_i,
            .sector_count = 10_i
        }(),
        .write_align = 1_i,
        .driver = storage::memory_storage_driver{}
    };

    constexpr auto partition_table = storage::partition_table{ .device = device }
        .append_partition(20_i)
        .append_partition()
    ;

    constexpr auto image_table = image::image_table
    {
        .boot = image::image_description
        {
            .primary = partition_table[0_i]
        },
        .applications = image::image_list
        {
            image::image_description
            {
                .primary = partition_table[1_i]
            }
        }
    };


    boost::ut::test("things") = [image_table]
    {
        constexpr auto boot_partition = image_table.boot.primary;
        constexpr auto app_partition = image_table.applications[0_i].primary;

        constexpr auto device = image_table.boot.primary.device;

        device.erase_all();

        std::array<unsigned char, 10> buffer{};
        boot_partition.write(0, buffer);
        app_partition.write(0, buffer);

        device.read(5, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);

        device.read(15, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0);

        app_partition.erase_all();

        device.read(5, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);

        device.read(15, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
    };
};


