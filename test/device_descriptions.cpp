#include <boost/ut.hpp>
#include <myboot/storage/device_description.hpp>
#include <myboot/storage/sector_list.hpp>

#include <myboot/literals/integral_constant.hpp>
#include <myboot/literals/byte_literals.hpp>

using namespace homestead::myboot::storage;


using namespace homestead::myboot::storage;
using namespace homestead::myboot::literals::bytes;
using namespace homestead::myboot::literals::integral;

boost::ut::suite device_descriptions = []
{
    boost::ut::test("basic_description") = []
    {
        constexpr auto device_tag0 = 0_i;
        constexpr auto device_tag1 = 1_i;

        constexpr auto device0 = device_description
        {
            .id = device_tag0,
            .sectors = even_sector_list
            {
                .sector_size = 10_i,
                .sector_count = 100_i
            }(),
            .write_align = 2_i,
            .driver = 0_i // not used in this test
        };
        
        boost::ut::expect(device0.total_size == 1000_i);
    };
};

