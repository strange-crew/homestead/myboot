#include <boost/ut.hpp>

#include <myboot/storage/device_description.hpp>
#include <myboot/storage/memory_storage_device.hpp>
#include <myboot/storage/partition_description.hpp>
#include <myboot/storage/partition_device.hpp>
#include <myboot/storage/sector_list.hpp>

#include <myboot/upgrade_strategies/simple_overwrite.hpp>

#include <random>
#include <array>

using namespace boost::ut;

using namespace homestead::myboot::storage;
using namespace homestead::myboot::upgrade_strategies;

auto random_device = std::random_device{};
auto distribution = std::uniform_int_distribution<unsigned char>(0X00, 0XFF);

template < typename Storage >
void fill_random(Storage & storage)
{
    storage.erase_all();

    std::array<unsigned char, 8> buffer;
    for (auto i = 0; i < storage.total_size(); i += buffer.size())
    {
        for (auto && ch : buffer)
            ch = distribution(random_device);

        storage.write(i, buffer);
    }
}


template < typename StorageA, typename StorageB >
int compare_storage(StorageA const& storage_a, StorageB const& storage_b)
{
    std::array<unsigned char, 8> buffer_a;
    std::array<unsigned char, 8> buffer_b;

    for (auto i = 0; i < storage_a.total_size(); i += buffer_a.size())
    {
        storage_a.read(i, buffer_a);
        storage_b.read(i, buffer_b);

        for (auto j = 0; j < 8; ++j)
        {
            auto cmp = buffer_a[j] - buffer_b[j];
            if (cmp != 0)
                return cmp;
        }
    }
    return 0;
}

suite simple_overwrite_tests = []
{
    constexpr auto small_device = device_description
    {
        .sectors = even_sector_list<{
            .sector_size = 16,
            .sector_count = 10
        }>{},
        .write_align = 4
    };

    "simple"_test = [small_device]
    {
        memory_storage_device<small_device> storage[2];

        fill_random(storage[0]);
        fill_random(storage[1]);

        expect(compare_storage(storage[0], storage[1]) != 0_i);

        simple_overwrite(storage[0] = storage[1]);
        expect(compare_storage(storage[0], storage[1]) == 0_i);
    };
};


