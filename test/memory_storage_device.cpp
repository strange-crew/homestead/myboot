#include <boost/ut.hpp>
#include <myboot/storage/memory_storage_device.hpp>
#include <myboot/storage/sector_list.hpp>

#include <myboot/literals/integral_constant.hpp>

using namespace homestead::myboot::storage;
using namespace homestead::myboot::literals::integral;

boost::ut::suite memory_storage = []
{
    auto storage = memory_storage_device
    {
        .description = device_description
        {
            .id = 0_i,
            .sectors = even_sector_list
            {
                .sector_size = 4096_i,
                .sector_count = 128_i
            }(),
            .write_align = 4_i,
            .driver = memory_storage_driver{}
        }
    };
    storage.erase_all();

    boost::ut::test("erase_all") = [storage]() mutable
    {
        std::array<unsigned char, 12> buffer;

        buffer[0] = 3;
        storage.write(0, buffer);

        storage.erase_all();
        storage.read(0, buffer);

        for (auto ch : buffer) boost::ut::expect(ch == 255_i);
    };

    boost::ut::test("write_erased") = [storage]() mutable
    {
        std::array<unsigned char, 12> buffer;
        for (auto i = 0; i < 12; ++i) buffer[i] = i;

        storage.write(0, buffer);
        for (auto i = 0; i < 12; ++i) buffer[i] = 3;

        storage.read(0, buffer);
        for (auto i = 0; i < 12; ++i)
        {
            boost::ut::expect(buffer[i] == i);
        }
    };

    boost::ut::test("write_written") = [storage]() mutable
    {
        std::array<unsigned char, 12> buffer;
        for (auto i = 0; i < 12; ++i)
        {
            auto value = i;
            value |= 1;
            value &= ~2;
            buffer[i] = value;
        }

        storage.write(0, buffer);

        for (auto i = 0; i < 12; ++i)
            buffer[i] = 3;

        storage.write(0, buffer);
        storage.read(0, buffer);
        for (auto && ch : buffer)
            boost::ut::expect(ch == 1);
    };

    boost::ut::test("write_align") = [storage]() mutable
    {
        std::array<unsigned char, 12> buffer;
        for (auto && ch : buffer) ch = 1;

        storage.write(0, buffer);
        std::array<unsigned char, 12> rbuf;
        storage.read(0, rbuf);
        for (auto i = 0; i < 12; ++i)
            boost::ut::expect(rbuf[i] == 1);

        storage.erase_all();
        storage.write(2, buffer);
        storage.read(0, rbuf);
        for (auto i = 0; i < 12; ++i)
            boost::ut::expect(rbuf[i] == 0xFF);

        storage.erase_all();
        storage.write(4, buffer);
        storage.read(0, rbuf);

        for (auto i = 0; i < 4; ++i)
            boost::ut::expect(rbuf[i] == 0xFF);
        for (auto i = 4; i < 12; ++i)
            boost::ut::expect(rbuf[i] == 1);
    };

    boost::ut::test("erase_sector") = [storage]() mutable
    {
        std::array<unsigned char, 12> buffer;
        for (auto && ch : buffer) ch = 1;
        storage.write(0, buffer);
        storage.read(0, buffer);
        for (auto i = 0; i < 12; ++i)
            boost::ut::expect(buffer[i] == 1);

        storage.erase_sector(0);
        storage.read(0, buffer);
        for (auto i = 0; i < 12; ++i)
        {
            std::cerr << (int)buffer[i] << "\n";
            boost::ut::expect(buffer[i] == 0xFF);
        }
    };
};

