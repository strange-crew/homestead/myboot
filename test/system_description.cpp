#include <boost/ut.hpp>

#include <myboot/hal/system_description.hpp>
#include <myboot/mcu/mcu_description.hpp>
#include <myboot/storage/sector_list.hpp>
#include <myboot/board/board_description.hpp>

#include <myboot/storage/memory_storage_device.hpp>

#include <algorithm>
#include <cassert>

using namespace homestead::myboot;
using namespace literals::integral;

template < typename Description >
struct mock_storage
{
    Description description;

    static std::array<unsigned char, Description{}.sectors.byte_size.value> buffer;
};

template < typename Description >
std::array<unsigned char, Description{}.sectors.byte_size.value> mock_storage<Description>::buffer;


struct mocked_driver
{
    template < typename Description >
    static void erase_all(Description description)
    {
        std::fill(std::begin(mock_storage<Description>::buffer), std::end(mock_storage<Description>::buffer), 0xFF);
    }
};

boost::ut::suite suite = []
{
    constexpr auto test_mcu = mcu::mcu_description
    {
        .arch = 0_i, // not used this test
        .storage_devices = storage::device_list
        {
            storage::device_description
            {
                .id = 0_i,
                .sectors = storage::even_sector_list
                {
                    .sector_size = 10_i,
                    .sector_count = 10_i
                }(),
                .write_align = 1_i,
                .driver = 0_i // not used this test
            }
        },
    };

    constexpr auto test_board = boards::board_description
    {
        .mcu = test_mcu,
        .peripherals = storage::device_list
        {
        }
    };


    boost::ut::test("initial state") = [test_board, test_mcu]
    {
        constexpr auto system = hal::build_hal(test_board);

        //system.starting_layout + 1;
        //test_board.mcu.storage_devices[0_i] + 1;

        boost::ut::expect(system.starting_layout.size == 1_i);
        boost::ut::expect(system.starting_layout[0_i].available_bytes == 100_i);

        constexpr auto table = system.starting_layout[0_i]
            .append_partition(10_i)
        ;

    };

    boost::ut::test("mocking") = [test_board]
    {
        constexpr auto original_hal = hal::build_hal(test_board);
        constexpr auto mocked = hal::mock_storage<storage::memory_storage_driver>(original_hal);

        boost::ut::expect(std::is_same<decltype(mocked.starting_layout[0_i].device.driver), storage::memory_storage_driver>::value);


        mocked.starting_layout[0_i].device.erase_all();

        auto && buffer = storage::memory_holder<decltype(mocked.starting_layout[0_i].device)>::storage.memory;
        boost::ut::expect(buffer[0] == 0xFF);
        boost::ut::expect(buffer.size() == 100);

        constexpr auto device = mocked.starting_layout[0_i].device;
        device.erase_sector(0);
        std::array<unsigned char, 8> buff;
        device.write(0, buff);
        device.read(0, buff);

    };
};
