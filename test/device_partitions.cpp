#include <boost/ut.hpp>
#include <myboot/storage/partition_description.hpp>
#include <myboot/storage/partition_device.hpp>
#include <myboot/storage/device_description.hpp>
#include <myboot/storage/sector_list.hpp>
#include <myboot/storage/memory_storage_device.hpp>
#include <myboot/literals/integral_constant.hpp>



using namespace homestead::myboot::literals::integral;
using namespace homestead::myboot;

boost::ut::suite partitions = []
{
    constexpr auto simple_device = storage::device_description
    {
        .id = 0_i,
        .sectors = storage::even_sector_list
        {
            .sector_size = 10_i,
            .sector_count = 10_i
        }(),
        .write_align = 2_i,
        .driver = storage::memory_storage_driver{}
    };

    boost::ut::test("simple") = [simple_device]
    {
        constexpr auto partition = storage::partition_description
        {
            .device = simple_device,
            .start_sector = 2_i,
            .end_sector = 5_i
        };

        boost::ut::expect(partition.byte_size == 40_i);
        boost::ut::expect(partition.sector_count == 4_i);
        boost::ut::expect(partition.offset == 20_i);

        simple_device.erase_all();

        std::array<unsigned char, 10> buffer{};
        partition.write(0, buffer);

        simple_device.read(0, buffer);
        for (auto i = 0; i < 10; ++i) boost::ut::expect(buffer[i] == 0xFF);

        simple_device.read(15, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
        for (auto i = 5; i < 10; ++i)
        {
            boost::ut::expect(buffer[i] == 0);
        }

        partition.read(5, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
    };

};


boost::ut::suite partition_tables = []
{
    constexpr auto simple_device = storage::device_description
    {
        .id = 0_i,
        .sectors = storage::even_sector_list
        { 
            .sector_size = 10_i,
            .sector_count = 10_i
        }(),
        .write_align = 1_i,
        .driver = storage::memory_storage_driver{} // not used this test
    };

    boost::ut::test("initial_state") = [simple_device]
    {
        constexpr auto empty_table = storage::partition_table{
            .device = simple_device
        };

        boost::ut::expect(empty_table.available_bytes == simple_device.total_size);
        boost::ut::expect(empty_table.reserved_bytes == 0_i);

    };

    boost::ut::test("single_partition") = [simple_device]
    {
        constexpr auto ptable = storage::partition_table{ .device = simple_device }
            .append_partition(30_i)
        ;

        boost::ut::expect(ptable[0_i].byte_size == 30_i);
        boost::ut::expect(ptable[0_i].start_sector == 0_i);
        boost::ut::expect(ptable[0_i].end_sector == 2_i);

        boost::ut::expect(ptable.available_sectors[0_i].index == 3_i);

        boost::ut::expect(ptable.available_bytes == 70_i);
        boost::ut::expect(ptable.reserved_bytes == 30_i);

    };

    boost::ut::test("fully_partitioned") = [simple_device]
    {
        constexpr auto ptable = storage::partition_table{ .device = simple_device }
            .append_partition(20_i)
            .append_partition()
        ;

        boost::ut::expect(ptable.available_bytes == 0_i);
        boost::ut::expect(ptable.reserved_bytes == 100_i);

        boost::ut::expect(ptable[0_i].byte_size == 20_i);
        boost::ut::expect(ptable[0_i].sector_count == 2_i);

        boost::ut::expect(ptable[1_i].byte_size == 80_i);
        boost::ut::expect(ptable[1_i].sector_count == 8_i);

        constexpr auto device = ptable.device;
        device.erase_all();

        std::array<unsigned char, 10> buffer{};
        ptable[0_i].write(0, buffer);
        ptable[1_i].write(0, buffer);

        device.read(5, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);

        device.read(15, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0);

        ptable[1_i].erase_all();

        device.read(5, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);

        device.read(15, buffer);
        for (auto i = 0; i < 5; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
        for (auto i = 5; i < 10; ++i)
            boost::ut::expect(buffer[i] == 0xFF);
    };

    boost::ut::test("uneven_partition") = [simple_device]
    {
        constexpr auto ptable = storage::partition_table{simple_device}
            .append_partition(33_i)
        ;

        boost::ut::expect(ptable.available_bytes == 60_i);
        boost::ut::expect(ptable.reserved_bytes == 40_i);

        boost::ut::expect(ptable[0_i].byte_size == 40_i);
        boost::ut::expect(ptable[0_i].sector_count == 4_i);
    };

};


#if 0
suite partitioned = []
{
    constexpr auto description = device_description
    {
        .sectors = even_sector_list<{
            .sector_size = 10,
            .sector_count = 100
        }>{},
        .write_align = 1
    };

    auto storage = memory_storage_device
    <
        description
    >{};

    storage.erase_all();

    auto partitioned = partition_device(storage)
        .append_partition<20>()
        .append_partition<30>()
    ;


    "initial_state"_test = [partitioned]() mutable
    {
        expect(partitioned.partition_count() == 2_i);
        expect(partitioned.get<0>().total_size() == 20_i);
        expect(partitioned.get<1>().total_size() == 30_i);

        expect(partitioned.get<1>().offset() == 20_i);

        //expect(partitioned[0].
    };

    "write"_test = [partitioned, &storage]() mutable
    {
        auto p0 = partitioned.get<0>();
        auto p1 = partitioned.get<1>();

        expect(p0.total_size() == 20);
        expect(p1.total_size() == 30);

        auto buffer = std::array<unsigned char, 10>{};
        p1.write(0, buffer);

        storage.read(0, buffer);
        for (auto ch : buffer) expect(ch == 255_i);

        p1.read(0, buffer);
        for (auto ch : buffer) expect(ch == 0_i);

        p0.write(0, buffer);
        storage.read(10, buffer);
        for (auto ch : buffer) expect(ch == 255_i);
        storage.read(0, buffer);
        for (auto ch : buffer) expect(ch == 0_i);

        storage.erase_all();
    };

    "erase_sector"_test = [partitioned, &storage]() mutable
    {
        auto p0 = partitioned.get<0>();
        auto p1 = partitioned.get<1>();

        auto buffer = std::array<unsigned char, 10>{};
        
        p0.write(0, buffer);
        p0.write(10, buffer);
        p1.write(5, buffer);

        p0.erase_sector(0);
        
        storage.read(0, buffer);
        for (auto ch : buffer) expect((int)ch == 255_i);

        storage.read(10, buffer);
        for (auto ch : buffer) expect((int)ch == 0_i);

        storage.read(20, buffer);
        for (auto i = 0; i < 5; ++i) expect((int)buffer[i] == 255_i);
        for (auto i = 5; i < 10; ++i) expect((int)buffer[i] == 0_i);

        storage.read(30, buffer);
        for (auto i = 0; i < 5; ++i) expect((int)buffer[i] == 0_i);
        for (auto i = 5; i < 10; ++i) expect((int)buffer[i] == 255_i);

        p1.erase_sector(0);

        storage.read(20, buffer);
        for (auto ch : buffer) expect((int)ch == 255_i);

        storage.read(30, buffer);
        for (auto i = 0; i < 5; ++i) expect((int)buffer[i] == 0_i);
        for (auto i = 5; i < 10; ++i) expect((int)buffer[i] == 255_i);
    };


    "erase_all"_test = [partitioned, &storage] () mutable
    {
        storage.erase_all();

        auto p0 = partitioned.get<0>();
        auto p1 = partitioned.get<1>();

        auto buffer = std::array<unsigned char, 10>{};
        
        p0.write(0, buffer);
        p0.write(10, buffer);
        p1.write(0, buffer);

        storage.read(0, buffer);
        for (auto ch : buffer) expect((int)ch == 0_i);

        storage.read(10, buffer);
        for (auto ch : buffer) expect((int)ch == 0_i);

        storage.read(20, buffer);
        for (auto ch : buffer) expect((int)ch == 0_i);

        p0.erase_all();

        storage.read(0, buffer);
        for (auto ch : buffer) expect((int)ch == 255_i);

        storage.read(10, buffer);
        for (auto ch : buffer) expect((int)ch == 255_i);

        storage.read(20, buffer);
        for (auto ch : buffer) expect((int)ch == 0_i);
    };
};

#endif
