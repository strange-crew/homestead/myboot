#include <boost/ut.hpp>
#include <myboot/storage/sector_list.hpp>
#include <myboot/literals/integral_constant.hpp>
#include <myboot/literals/byte_literals.hpp>


//using namespace boost::ut;
using namespace homestead::myboot::storage;
using namespace homestead::myboot::literals::integral;
using namespace homestead::myboot::literals::bytes;

using namespace homestead::myboot::meta;

boost::ut::suite sectors = []
{
    boost::ut::test("simple") = []
    {
        constexpr auto sector = sector_description
        {
            .index = 0_i,
            .offset = 0_i,
            .size = 10_i
        };
    };

    boost::ut::test("sector_list") = []
    {
        constexpr auto list = sector_list
        {
            sector_description{ .index = 0_i, .offset = 0_i, .size = 10_i },
            sector_description{ .index = 1_i, .offset = 10_i, .size = 10_i }
        };
        constexpr auto appended = list.append(
                sector_description{ .index = 2_i, .offset = 20_i, .size = 10_i },
                sector_description{ .index = 3_i, .offset = 30_i, .size = 10_i }
            )
        ;
        constexpr auto empty = sector_list{};


        boost::ut::expect(list[0_i].index == 0_i);
        boost::ut::expect(list[1_i].index == 1_i);
        boost::ut::expect(appended[2_i].index == 2_i);

        boost::ut::expect(list.sector_count == 2_i);
        boost::ut::expect(appended.sector_count == 4_i);
        boost::ut::expect(empty.sector_count == 0_i);

        boost::ut::expect(list.byte_size == 20_i);
        boost::ut::expect(appended.byte_size == 40_i);
        boost::ut::expect(empty.byte_size == 0_i);

        boost::ut::expect(appended[3_i].size == 10_i);
        boost::ut::expect(appended[1_i].offset == 10_i);
        boost::ut::expect(appended[2_i].offset == 20_i);
    };


    boost::ut::test("even_list") = []
    {
        constexpr auto list = even_sector_list
        {
            .sector_size = 10_i,
            .sector_count = 10_i
        }();

        boost::ut::expect(list.byte_size == 100_i);
        boost::ut::expect(list.sector_count == 10_i);
        boost::ut::expect(list[3_i].offset == 30_i);
        boost::ut::expect(list[5_i].index == 5_i);
    };


#if 0
    boost::ut::test("render_item") = []
    {
        constexpr auto list =* even_sector_list
        {
            .sector_size = 10_i,
            .sector_count = 10_i
        };
        struct sector { int i, o, s; };
        constexpr auto from = sector_description
        {
            .index = 0_i,
            .offset = 0_i,
            .size = 10_i
        };
        //constexpr auto rendered_sector = render_item<sector>(list[1_i]);
        constexpr auto rendered_sector = render_item<sector>(from);

        boost::ut::expect(rendered_sector.i == 0);
        boost::ut::expect(rendered_sector.o == 0);
        boost::ut::expect(rendered_sector.s == 10);
    };


    boost::ut::test("render_list") = []
    {
        constexpr auto list =* even_sector_list
        {
            .sector_size = 10_i,
            .sector_count = 10_i
        };

        struct full_sector { int i, o, s; };

        constexpr auto full_render = render_list<full_sector>(list);

        boost::ut::expect(full_render[0].i == 0);
        boost::ut::expect(full_render[0].o == 0);
        boost::ut::expect(full_render[0].s == 10);
        boost::ut::expect(full_render[1].i == 1);
        boost::ut::expect(full_render[1].o == 10);
        boost::ut::expect(full_render[1].s == 10);

        struct different_desc
        {
            int start;
            int end;
        };
        constexpr auto diff_render = render_list_f(list, 
                [](auto item) constexpr
                {
                    return different_desc{ item.offset.value, (item.offset + item.size).value };
                }
            )
        ;

        boost::ut::expect(diff_render[5].start == 50);
        boost::ut::expect(diff_render[5].end == 60);
    };
#endif
};

#if 0
boost::ut::suite device_descriptions = []
{
    boost::ut::test("even_sector_list") = []
    {
        constexpr auto sector_list =* even_sector_list
        {
            .sector_size = 4096_i,
            .sector_count = 128_i
        };
        constexpr auto total_size = sector_list.total_size();
        constexpr auto flash_size = 512_k;

        boost::ut::expect(total_size.value() == flash_size.value());

        constexpr auto sector0 = sector_list[0_i];
        constexpr auto sector22 = sector_list[22_i];

        boost::ut::expect(sector0.index() == 0_i);
        boost::ut::expect(sector22.index() == 22_i);
        boost::ut::expect(sector0.offset() == 0_i);
        boost::ut::expect(sector22.offset() == 4096_i * 22_i);
        boost::ut::expect(sector0.size() == sector22.size());
        boost::ut::expect(sector0.size() == 4096_i);

        boost::ut::expect(sector_list.sector_count() == 128_i);

        {
            constexpr auto tpl = sector_list.reserve_from(0_i, 32_k);
            constexpr auto start = std::get<0>(tpl);
            constexpr auto end = std::get<1>(tpl);

            boost::ut::expect(start.offset() == 0_i);
            std::cerr << end.offset().value() << " == " << (32_k - end.size()).value() << "\n";
            boost::ut::expect(end.offset() == 32_k - end.size());
            boost::ut::expect(start.index() == 0_i);
            std::cerr << end.index().value() << " == " << (7_i).value() << "\n";
            boost::ut::expect(end.index() == 7_i);
            boost::ut::expect(end.index() * end.size() + end.size() == 32_k);
        }
        {
            constexpr auto tpl = sector_list.reserve_from(1_i, 32_k);
            constexpr auto start = std::get<0>(tpl);
            constexpr auto end = std::get<1>(tpl);

            boost::ut::expect(start.index() == 1_i);
            boost::ut::expect(start.offset() == start.size());
            boost::ut::expect(end.index() == 8_i);
            boost::ut::expect(end.offset() == 32_k);
        }
    };
};


boost::ut::suite runtime_pull = []
{
    boost::ut::test("sector values") = []
    {
        //constexpr auto sect = sector_
    };


    boost::ut::test("sector lists") = []
    {
    };
};

#endif
