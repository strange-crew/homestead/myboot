
//#include <myboot/literals/integral_constant.hpp>
//

#include <myboot/mcu/nrf52/nrf52832.hpp>
#include <myboot/storage/device_description.hpp>
#include <myboot/board/board_description.hpp>
#include <myboot/hal/system_description.hpp>
#include <myboot/image/image_table.hpp>
#include <myboot/image/image_description.hpp>

#include <myboot/literals/byte_literals.hpp>

#include <myboot/storage/memory_storage_device.hpp>

#include <cstdint>

using namespace homestead::myboot::literals::integral;
using namespace homestead::myboot::literals::bytes;
using namespace homestead::myboot;



constexpr auto pinetime_description = boards::board_description
{
    .mcu = mcu::nrf52832,
    .peripherals = storage::device_list
    {
    }
};





constexpr auto sys = hal::build_hal(pinetime_description);
// when testing...
//constexpr auto sys = hal::mock_storage<storage::memory_storage_driver>(hal::build_hal(pinetime_description));

// Now hal has .storage_devices -- is this a list of devices or partitions?

// Now we create storage layout based on the hal
constexpr auto starting_layout = sys.starting_layout;

constexpr auto system_layout = storage::partition_list
{
    starting_layout[0_i]
        .append_partition(32_k)
        .append_partition()
};


constexpr auto boot_image = image::image_description
{
    .primary = system_layout[0_i][0_i]
};

constexpr auto applications = image::image_list
{
    image::image_description
    {
        .primary = system_layout[0_i][1_i]
    }
};

constexpr auto images = image::image_table
{
    .boot = image::image_description
    {
        .primary = system_layout[0_i][0_i]
    },
    .applications = image::image_list
    {
        image::image_description
        {
            .primary = system_layout[0_i][1_i]
        }
    }
};


template < typename Partition >
void boot_partition(storage::drivers::flashpage_t driver, Partition partition)
{
}

#ifndef NRF52
#include <iostream>
template < typename Partition >
void boot_partition(storage::memory_storage_driver driver, Partition partition)
{
    std::array<unsigned char, 128> buffer{};
    for (auto i = 0; i < partition.byte_size; i += 128)
    {
        partition.read(i, buffer);
        std::cout.write(reinterpret_cast<char*>(buffer.data()), 128);
    }
}
#endif


template < typename Partition >
void boot_partition(Partition partition)
{
    boot_partition(partition.device.driver, partition);
}
//#include <string>
int main()
{
#if 0
    auto && storage = storage::memory_holder<decltype(system_layout[0_i].device)>::storage;
    auto message = std::string("hello world");
    message.push_back('\0');

    storage.erase_all();

    storage.write((48_k).value, message);

#endif
    boot_partition(images.applications[0_i].primary);

#ifdef NRF52
    //images.applications[0_i].boot();
    //
    char buffer[] = "Hello olleHello";
    std::uint32_t args[3] = {
        1,
        (std::uint32_t)buffer,
        (std::uint32_t)sizeof(buffer)
    };
    int cmd = 5;


        std::uint32_t result = 0;

        __asm__(
                "mov r0, %[cmd] \n"
                "mov r1, %[args] \n"
                "bkpt 0xAB \n"
                "mov %[result], r0 \n"
                :
                [result] "=r" (result)
                :
                [cmd] "r" (cmd),
                [args] "r" (args)
                :
                "r0", "r1", "memory"
            )
        ;
        cmd = result;

    while (true)
    {
    }
#endif
    
}

#if 0


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include <mbedtls/ctr_drbg.h>
//#include <mbedtls/entropy.h>
#include <mbedtls/rsa.h>
#include <mbedtls/pk.h>
#include <mbedtls/x509_csr.h>
#include <mbedtls/x509_crt.h>

#if 0
#include <nrf_drv_rng.h>

int rnd(void*, unsigned char * buf, size_t len)
{
    while (len > 0)
    {
        uint8_t available = 0;
        nrf_drv_rng_bytes_available(&available);
        if (available > 0)
        {
            uint8_t readlen = MIN(available, len);
            if (nrf_drv_rng_rand(buf, readlen) != NRF_SUCCESS)
                return -1;
            len -= readlen;
            buf += readlen;
        }
        if (len > 0) for (auto i = 0; i < 100; ++i);
    }
    return 0;
}
#endif

int main() 
{
    //nrf_drv_rng_init(nullptr);
    mbedtls_pk_context pk;
    mbedtls_pk_init(&pk);
    mbedtls_pk_setup(&pk, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));

    //mbedtls_rsa_context * rsap = mbedtls_pk_rsa(pk);
    mbedtls_rsa_context rsa; //&rsa = *rsap;
    mbedtls_rsa_init(&rsa);
    mbedtls_rsa_set_padding(&rsa, MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);

#if 0
    puts("generating key");
    auto result = mbedtls_rsa_gen_key(&rsa, rnd, nullptr, 2048, 0x10001);
    if (result != 0)
        printf("error: %x\n", -result);

    unsigned char * buf = (unsigned char*)malloc(5000);
    mbedtls_pk_write_pubkey_pem(&pk, buf, 5000);
    puts((char*)buf);
    puts("done");
#endif
#if 0
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context random;

    puts("building entropy");

    mbedtls_entropy_init(&entropy);
    {
        auto result = mbedtls_entropy_gather(&entropy);
        if (result != 0)
            printf("error: %x\n", -result);
    }

    puts("building random");
    mbedtls_ctr_drbg_init(&random);

    puts("seeding random");
    auto result = mbedtls_ctr_drbg_seed(&random, mbedtls_entropy_func, &entropy, nullptr, 0);
    if (result != 0)
        printf("error: %x\n", -result);

    puts("building rsa");
    mbedtls_pk_context pk;
    mbedtls_pk_init(&pk);
    mbedtls_pk_setup(&pk, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));

    mbedtls_rsa_context * rsap = mbedtls_pk_rsa(pk);
    mbedtls_rsa_context &rsa = *rsap;
    mbedtls_rsa_init(&rsa);
    mbedtls_rsa_set_padding(&rsa, MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);

    puts("generating key");
    result = mbedtls_rsa_gen_key(&rsa, mbedtls_ctr_drbg_random, &random, 2048, 0x10001);
    if (result != 0)
        printf("error: %x\n", -result);

    puts("printing key");
    unsigned char * buf = (unsigned char*)malloc(5000);
    mbedtls_pk_write_pubkey_pem(&pk, buf, 5000);
    std::cout << buf << "\n";

    std::cout << "generating csr\n";
    mbedtls_x509write_csr csr;
    mbedtls_x509write_csr_init(&csr);
    mbedtls_x509write_csr_set_key(&csr, &pk);
    mbedtls_x509write_csr_set_subject_name(&csr, "CN=watch");
    mbedtls_x509write_csr_set_md_alg(&csr, MBEDTLS_MD_SHA256);

    result = mbedtls_x509write_csr_pem(&csr, buf, 5000, mbedtls_ctr_drbg_random, &random);
    if (result != 0) std::cerr << "error: " << result << "\n";
    std::cout << buf << "\n";

    std::cerr << "generating self-signed certificate\n";
    mbedtls_x509write_cert crt;
    mbedtls_x509write_crt_init(&crt);
    mbedtls_x509write_crt_set_subject_name(&crt, "CN=watch");
    mbedtls_x509write_crt_set_subject_key(&crt, &pk);
    mbedtls_x509write_crt_set_issuer_name(&crt, "CN=watch");
    mbedtls_x509write_crt_set_issuer_key(&crt, &pk);
    mbedtls_x509write_crt_set_md_alg(&crt, MBEDTLS_MD_SHA256);

    mbedtls_mpi serial;
    mbedtls_mpi_init(&serial);
    mbedtls_mpi_lset(&serial, 1);
    mbedtls_x509write_crt_set_serial(&crt, &serial);
    mbedtls_x509write_crt_set_validity(&crt, "20220101000000", "20220120000000");

    mbedtls_x509write_crt_set_basic_constraints(&crt, true, 10);
    mbedtls_x509write_crt_set_subject_key_identifier(&crt);
    mbedtls_x509write_crt_set_authority_key_identifier(&crt);

    result = mbedtls_x509write_crt_pem(&crt, buf, 5000, mbedtls_ctr_drbg_random, &random);
    if (result != 0) std::cerr << "error: " << result;
    else std::cerr << buf << "\n";

    mbedtls_x509_crt check_crt;
    mbedtls_x509_crt_init(&check_crt);
    result = mbedtls_x509_crt_parse(&check_crt, buf, strlen((char*)buf));
    if (result != 0) std::cerr << "error: " << result << "\n";

    uint32_t flags = 0;
    result = mbedtls_x509_crt_verify(&check_crt, &check_crt, nullptr, nullptr, &flags, nullptr, nullptr);
    if (result != 0)
    {
        mbedtls_x509_crt_verify_info((char*)buf, 5000, "", flags);
        std::cerr << buf << "\n";
    }
    std::cout << result << "\n";
#endif
}


#endif
