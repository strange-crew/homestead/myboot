#!/bin/bash

set -x

multilibs=$(/build/nano-install-tmp/bin/arm-none-eabi-gcc -print-multi-lib 2>/dev/null)
for multilib in ${multilibs[@]}; do
    multidir=$(echo ${multilib} | awk '{split($0,a,";"); print a[1]}')
    libs=(/build/nano-install-tmp/arm-none-eabi/lib/${multidir}/*.a)
    specs=(/build/nano-install-tmp/arm-none-eabi/lib/${multidir}/*.specs)

    for lib in "${libs[@]}"; do
        name=$(basename ${lib} .a)
        cp ${lib} /opt/arm-none-eabi/arm-none-eabi/lib/${multidir}/${name}_nano.a
    done

    for spec in "${specs[@]}"; do
        name=$(basename ${spec} .specs)
        cp  ${spec} /opt/arm-none-eabi/arm-none-eabi/lib/${multidir}/${name}.specs
    done
done

mkdir -p /opt/arm-none-eabi/arm-none-eabi/include/newlib-nano
cp /build/nano-install-tmp/arm-none-eabi/include/newlib.h /opt/arm-none-eabi/arm-none-eabi/include/newlib-nano/newlib.h

